import os
import argparse

import librosa
import numpy as np
from pydub import AudioSegment


def ExtraFeat(inputs):

    inputs=inputs
    fs = 22050
    n_fft=1024
    hop_len=256
    n_mels=80
    
    print('Loading wav file from {}'.format(inputs))
    x, sr = librosa.load(inputs, sr=fs)
    raw = AudioSegment.from_file(inputs, 'wav')

    assert int(raw.duration_seconds) == int(librosa.get_duration(x, sr=fs)),'Music length mismatch: raw ({}) != loaded ({})'.format(raw.duration_seconds, librosa.get_duration(x, sr=fs))

    logmel = np.log(librosa.feature.melspectrogram(x, sr=fs, n_fft=n_fft, hop_length=hop_len, n_mels=n_mels))
    print('Extracted {} features'.format(logmel.shape))


    outfile="./test.npy"

    np.save(outfile, logmel)

    return logmel


