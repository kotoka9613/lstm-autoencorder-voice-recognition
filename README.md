# lstm-autoencorder-voice-recognition

# 環境構築
```
git clone https://gitlab.com/kotoka9613/lstm-autoencorder-voice-recognition.git
pip install requirement.txt
```
<br>

# 表示確認
1. ローカルでjudge.htmlを開く
1. "ファイルを開く"をからresult.jsonを選択
1. 画面が切り替わり、以下のような画像が出力されていることを確認
![test](./data/image/test.png)

<br>

# 動作確認
```
python judge.py --wav ./data/test.wav
```
- test.npy, result.jsonが出力される
