// ページがロードされたら呼ばれる
$(document).ready(function() {
    var reader; // jsonファイルを読み込むためのリーダー
    reader = new FileReader();

    // inputタグでjsonファイルが選択されたら呼ばれる
    $("#jsonfile").on('change', function(event){
        reader.readAsText(event.target.files[0]);
    });

    // readerにファイルが読み込まれたら呼ばれる
    reader.onload = function(event) {
        data = JSON.parse(event.target.result); // jsonをパース
        console.log(data.true);
        var true_list=[]
        var false_list=[]
        var a

        // 音楽を埋め込み
        music_path = data.wav
        $("#audiowrapper").append($('<audio id ="video" preload="metadata" controls> <source src=' + music_path + ' type="audio/wav"></audio>'));
        $("#canvaswrapper").prepend($('<canvas id="canvas" width="' + $("#video").width() + '" height="300px"></canvas>'));
        $("#image").append($('<img src="data/image/bish_1.jpeg" width="60%" height="30%" class="image">'));
        $("#framelist").append("<h1>" +"AVERAGE LOSS"+ "</h1>","<h1>" + data.loss  + "</h1>");

        var v = document.getElementById("video");
        v.addEventListener('timeupdate', function (e) {
            for (var i = 0; i < data.true.length; i++) {
                if (data.true[i][0]<=v.currentTime&&v.currentTime<=data.true[i][1]) {
                    a=1
                }
            }
            for (var i = 0; i < data.false.length; i++) {
                if (data.false[i][0]<=v.currentTime&&v.currentTime<=data.false[i][1]) {
                    a=0;
                }
            }

          if(a===0){
              $('.image').remove()
              $("#image").append($('<img src="data/image/other.png" width="60%" height="30%" class="image">'));
          }else if(a===1){
              $('.image').remove()
              $("#image").append($('<img src="data/image/bish_1.jpeg" width="60%" height="30%" class="image">'));
          }

        });

        gchart = new GanttChart($("canvas").width());

        for(let i = 0; i < data.true.length; i++) {
          gchart.drawSeqBar(data.true[i][0] / 205, data.true[i][1] / 205, true);
        }

        for(let i = 0; i < data.false.length; i++) {
          gchart.drawSeqBar(data.false[i][0] / 205, data.false[i][1] / 205, false);
        }
    };
});