var GanttChart = (function() {
    var GanttChart = function(width) {
        var _width = width;
        var _height = 80;
        var _offsetl =140;
        var _offsetr =58;

        $("#canvaswrapper").prepend($('<canvas id="canvas" width=' + _width * 2 + ' height=' + _height * 2 + ' style="width:' + _width + 'px; height: ' + _height + 'px;"></canvas>'));
        var _ctx = document.getElementById('canvas').getContext('2d');

        _ctx.scale(2, 2)

        _ctx.fillStyle = "rgb(255, 255, 255)";
        _ctx.fillRect(0, 0, _width, _height / 2);
        _ctx.fillStyle = "rgb(245, 245, 245)";
        _ctx.fillRect(0, _height / 2, _width, _height);
        _ctx.strokeStyle = "rgb(224, 224, 224)";
        _ctx.lineWidth = 1;
        _ctx.beginPath();
        _ctx.moveTo(0, _height / 2);
        _ctx.lineTo(_width, _height / 2);
        _ctx.stroke();

        _ctx.fillStyle = "rgb(0, 0, 0)";
        _ctx.font = "20px arial";
        _ctx.fillText("BiSH", 45, 28);
        _ctx.fillText("Other", 43, 67);

        Object.defineProperties(this, {
            ctx: {
                get: function() {
                    return _ctx;
                }
            },
            width: {
                get: function() {
                    return _width;
                }
            },
            height: {
                get: function() {
                    return _height;
                }
            },
            offsetl: {
                get: function() {
                    return _offsetl;
                }
            },
            offsetr: {
                get: function() {
                    return _offsetr;
                }
            }
        });
    }

    var p = GanttChart.prototype;

    p.drawSeqBar = function(t0, t1, is_bish) {
        var self = this;

        len_play_bar = self.width - (self.offsetl + self.offsetr);
        t0 *= len_play_bar;
        t1 *= len_play_bar;

        x0 = t0 + self.offsetl;
        y0 = (is_bish)? 8: 48;
        w = t1 - t0;
        h = 24;
        r = 4;
        alpha = (is_bish)? 1.0: 0.5;

        self.ctx.beginPath();
        self.ctx.moveTo(x0 + r, y0);
        self.ctx.arcTo(x0 + w, y0, x0 + w, y0 + r, r);
        self.ctx.arcTo(x0 + w, y0 + h, x0 + w - r, y0 + h, r);
        self.ctx.arcTo(x0, y0 + h, x0, y0 + h - r, r);
        self.ctx.arcTo(x0, y0, x0 + r, y0, r);
        self.ctx.fillStyle = 'rgb(23, 86, 194)';
        self.ctx.globalAlpha = alpha;
        self.ctx.fill();
        self.ctx.globalAlpha = 1;
        self.ctx.strokeStyle = 'rgb(23, 86, 194)';
        self.ctx.lineWidth = 1;
        self.ctx.stroke();
    }

    return GanttChart;
})();