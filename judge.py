import torch

import time
from torch import nn
from torch.utils.data import Dataset
from torch.utils.data import random_split, DataLoader
import glob
import numpy as np
from torch.optim import SGD
from torch.optim import Adam
import os
import random
import argparse
import extract_feat
import json
import itertools

class Predictor(nn.Module):
    def __init__(self, inputDim, hiddenDim, outputDim):
        super(Predictor, self).__init__()

        self.rnn = nn.LSTM(input_size = inputDim,
                            hidden_size = hiddenDim,
                           batch_first=True)
        self.output_layer = nn.Linear(hiddenDim, outputDim)
    
    def forward(self, inputs, hidden0=None):
        output, (hidden, cell) = self.rnn(inputs, hidden0) #LSTM層
        output = self.output_layer(output[:, -1, :]) #全結合層

        return output
    
    def encode(self, inputs):
        self.eval()
        output, (hidden, cell) = self.rnn(inputs)
        return output

class MyDataset_file(Dataset):
    def __init__(self, data_path, seq_len=50):
        self.data_path = data_path
        self.data = []
        self.label = []
        self.seq_idx = []
        self.color=[]

        idx = 0
        for i, path in enumerate(glob.glob(data_path)):
          d = np.load(path).astype(np.float32).T
          self.data.append(d)
          self.seq_idx.extend([(i, i + seq_len) for i in range(idx, idx + d.shape[0] - seq_len)])
          # [(0, 50), (1, 51), (2, 52)]
          idx += d.shape[0]
          self.label.extend([i for j in range(d.shape[0] - seq_len)])
          basename = os.path.splitext(os.path.basename(path))[0]
          print(basename)
        self.data = np.vstack(self.data)

    def __len__(self):
        return len(self.seq_idx)

    def __getitem__(self, idx):
        return self.data[self.seq_idx[idx][0]: self.seq_idx[idx][1]], self.label[idx]

class MyDataset(Dataset):
    def __init__(self, data_path, seq_len=50):
        self.d = data_path
        self.data = []
        self.label = []
        self.seq_idx = []
        self.color=[]

        idx = 0
        for i, path in enumerate(glob.glob(os.path.join(data_path, '*.npy'))):
          d = np.load(path).astype(np.float32).T
          self.data.append(d)
          self.seq_idx.extend([(i, i + seq_len) for i in range(idx, idx + d.shape[0] - seq_len)])
          # [(0, 50), (1, 51), (2, 52)]
          idx += d.shape[0]
          self.label.extend([i for j in range(d.shape[0] - seq_len)])
          basename = os.path.splitext(os.path.basename(path))[0]
          print(basename,i)
        self.data = np.vstack(self.data)

    def __len__(self):
        return len(self.seq_idx)

    def __getitem__(self, idx):
        return self.data[self.seq_idx[idx][0]: self.seq_idx[idx][1]], self.label[idx]

class Autoencoder(nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(300,128),
            nn.ReLU(True),
            nn.Linear(128, 64)
        )

        self.decoder = nn.Sequential(
            nn.Linear(64, 128),
            nn.ReLU(True),
            nn.Linear(128, 300)
        )

    def forward(self, x):
        z = self.encoder(x)
        xhat = self.decoder(z)
        return xhat


parser = argparse.ArgumentParser()
parser.add_argument('--wav', type=str, required=True)
args = parser.parse_args()



lstm_model = Predictor(80,300,20)
device = torch.device('cpu')

lstm_model.to(device)
lstm_model.load_state_dict(torch.load("./model_dir/50_bish_lstm_cpu.pth"))

auto_model=Autoencoder().to(device)
auto_model.load_state_dict(torch.load("./model_dir/50_bish_auto_cpu.pth"))

feat_data=extract_feat.ExtraFeat(args.wav)
path="./test.npy"
dataset = MyDataset_file(path)
dataloader_train = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0, pin_memory=True)
basename = os.path.splitext(os.path.basename(path))[0]
# 最小2乗誤差
criterion = nn.MSELoss()
lstm_model.eval()
auto_model.eval()
losses=0
t=25
tag=True
result=[]
result_true=[]
result_false=[]
true = []
false= []
label=[]
i=0
j=0
for data, label in dataloader_train:
  data = data.to(device)
  # import pdb; pdb.set_trace()
  out=lstm_model.encode(data)
  out=out[:,-1]
  out2=auto_model(out)
  loss = criterion(out, out2)
  losses+=loss.item()
  
  if loss.item()<0.0325:
    tag=1
    result.append(tag)
  else:
    tag=0
    result.append(tag)
i=0
t=0
b=0
gr = itertools.groupby(result)


for key, group in gr:
  if key==1:
    if b==0:
        group_list=list(group)
      #   len(group_list)+25を秒に変換
        s=(len(group_list)+25)*0.0116
        t=i+s
        t1=round(t)
        i1=round(i)
        if t1!=i1:
            label=[i1, t1]
            true.append(label)
        i=t
        b+=1
        
    else:
        group_list=list(group)
        s=len(group_list)*0.0116
        t=i+s
        t1=round(t)
        i1=round(i)
        if t1!=i1:
            label=[i1, t1]
            true.append(label)
        i=t
  if key==0:
    if b==0:
        group_list=list(group)
      #   len(group_list)+25を秒に変換
        s=(len(group_list)+25)*0.0116
        t=i+s
        t1=round(t)
        i1=round(i)
        if t1!=i1:
            label=[i1, t1]
            false.append(label)
        i=t
        b+=1
        
    else:
        group_list=list(group)
        s=len(group_list)*0.0116
        t=i+s
        t1=round(t)
        i1=round(i)
        if i1!=t1:
            label=[i1, t1]
            false.append(label)
        i=t
        
false1=[]
true1=[]
tmp=[]

i=0
j=0
for a in range(len(false)):
    if false[j][1]==false[j+1][0]:
        tmp=[false[j][0],false[j+1][1]]
        false[j+1]=tmp
        j+=1
        i=1
        
    else:
        false1.append(false[j])
        i=0
        j+=1
        
    if j==len(false)-1:
        false1.append(false[j])
        break
j=0
i=0
for a in range(len(true)):
    if true[j][1]==true[j+1][0]:
        tmp=[true[j][0],true[j+1][1]]
        true[j+1]=tmp
        i=1
        j+=1
        
    else:
        true1.append(true[j])
        i=0
        j+=1
        
    if j==len(true)-1:
        true1.append(true[j])
        break
  

avr_loss=losses/len(dataloader_train)
result_true={"wav":args.wav,"loss":avr_loss,"frame":true1}
result_false={"wav":args.wav,"loss":avr_loss,"frame":false1}
result={"wav":args.wav,"loss":avr_loss,"true":true1,"false":false1}
print("loss : {}".format(avr_loss))


with open('./result.json', 'w') as f:
    json.dump(result, f)